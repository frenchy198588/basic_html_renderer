var express = require('express')
const mysql = require('mysql');
const path = require('path');

var app = express()

const db = mysql.createConnection({
    host: '192.168.99.100',
    user: 'root',
    password: 'password1',
    database: 'test'
});

db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

app.set('port', 8080); // set express to use this port
app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use("/list", function (req, res) {

    db.query("select firstname, surname from user", function (err, result) {
        // var list = []

        // for( var i = 0; i < result.length; i++) {
        //     list.push(result[i].email)
        // }

        // var output = "<html><head></head><body><h1>Hello World</h1><ul>"

        // for (var i = 0; i < list.length; i++) {
        //     output += "<li>" + list[i] + "</li>"
        // }

        // output += "</ul></body></html>"

        // res.send(output)

        res.render('list.ejs', {
            data: result
        });
    })
})

app.listen(8080)